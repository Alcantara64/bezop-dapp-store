<!-- PAGE -->
<section class="page-section featured-products">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="section-title section-title-lg">
                            <span>
                                <span class="thin"> <?php echo translate('today\'s_deal');?></span>
                            </span>
                        </h2>
                        <div class="featured-products-carousel">
                            <div class="owl-carousel-2" id="todays-deal-carousel">
                                <?php
                                    $limit =  $this->db->get_where('ui_settings',array('ui_settings_id' => 30))->row()->value;
                                    $todays_deal=$this->crud_model->product_list_set('deal',$limit);
                                    foreach($todays_deal as $row){
                                        echo $this->html_model->product_box($row, 'grid', '2');
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="section-title section-title-lg">
                            <span>
                                <span class="thin"> <?php echo translate('latest');?></span>
                                <?php echo translate('featured');?> 
                                <span class="thin"> <?php echo translate('product');?></span>
                            </span>
                        </h2>
                        <div class="featured-products-carousel">
                            <div class="owl-carousel" id="featured-products-carousel">
                                <?php
                                    $box_style =  $this->db->get_where('ui_settings',array('ui_settings_id' => 29))->row()->value;
                                    $limit =  $this->db->get_where('ui_settings',array('ui_settings_id' => 20))->row()->value;
                                    $featured=$this->crud_model->product_list_set('featured',$limit);
                                    foreach($featured as $row){
                                        echo $this->html_model->product_box($row, 'grid', $box_style);
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="section-title section-title-lg">
                            <span>
                                <span class="thin"> <?php echo translate('latest');?></span>
                                <span class="thin"> <?php echo translate('product');?></span>
                            </span>
                        </h2>
                        <div class="featured-products-carousel">
                            <div class="owl-carousel-3" id="latest-product-carousel">
                                <?php
                                    $latest=$this->crud_model->product_list_set('latest',5);
                                    foreach($latest as $row){
                                        echo $this->html_model->product_box($row, 'grid', '2');
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="section-title section-title-lg">
                            <span>
                                <span class="thin"> <?php echo translate('recently');?></span>
                                <span class="thin"> <?php echo translate('viewed');?></span>
                            </span>
                        </h2>
                        <div class="featured-products-carousel">
                            <div class="owl-carousel-4" id="recently-viewed-carousel">
                                <?php
                                    $recently_viewed=$this->crud_model->product_list_set('recently_viewed',5);
                                    foreach($recently_viewed as $row){
                                        echo $this->html_model->product_box($row, 'grid', '2');
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h2 class="section-title section-title-lg">
                            <span>
                                <span class="thin"> <?php echo translate('most');?></span>
                                <span class="thin"> <?php echo translate('viewed');?></span>
                            </span>
                        </h2>
                        <div class="featured-products-carousel">
                            <div class="owl-carousel-5" id="most-viewed-carousel">
                                <?php
                                    $most_viewed=$this->crud_model->product_list_set('most_viewed',5);
                                    foreach($most_viewed as $row){
                                        echo $this->html_model->product_box($row, 'grid', '2');
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <?php
                    $categories = json_decode($this->db->get_where('ui_settings',array('type'=>'home_categories'))->row()->value,true);
                    foreach($categories as $row) {
                        if($this->crud_model->if_publishable_category($row['category'])) {
                    ?>
                        <?php 
                            if(file_exists('uploads/category_image/'.$this->crud_model->get_type_name_by_id('category',$row['category'],'banner'))){
                                $img_url = base_url()."uploads/category_image/".$this->crud_model->get_type_name_by_id('category',$row['category'],'banner'); 
                            } else {
                                $img_url = base_url()."uploads/category_image/default.jpg"; 

                            }
                        ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div style="height: 250px; width: 100%; background-image: url(<?=$img_url?>); background-position-x: center; background-position-y: top; background-repeat: no-repeat;">
                                    <div class="p-item p-item-type-zoom">
                                        <span class="p-item-hover">
                                            <div class="p-item-info">
                                                <div class="p-headline">
                                                    <span><?php echo $this->crud_model->get_type_name_by_id('category',$row['category'],'category_name'); ?></span>
                                                    <div class="p-line"></div>
                                                    <div class="p-btn">
                                                        <a href="<?php echo base_url(); ?>home/category/<?php echo $row['category']; ?>" class="btn  btn-theme-transparent btn-theme-xs"><?php echo translate('browse'); ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="p-mask" style="height: 250px"></div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                }
                ?>
            </div>
        </div>
        
    </div>
</section>
<!-- /PAGE -->
<script>
$(document).ready(function(){
    $(".owl-carousel-2").owlCarousel({
        autoplay: true,
        loop: true,
        margin: 30,
        dots: false,
        nav: true,
        navText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
        responsive: {
            0: {items: 2},
            479: {items: 2},
            768: {items: 2},
            991: {items: 4},
            1024: {items: 4}
        }
    });

    $(".owl-carousel-3").owlCarousel({
        autoplay: true,
        loop: true,
        margin: 30,
        dots: false,
        nav: true,
        navText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
        responsive: {
            0: {items: 2},
            479: {items: 2},
            768: {items: 2},
            991: {items: 4},
            1024: {items: 4}
        }
    });

    $(".owl-carousel-4").owlCarousel({
        autoplay: true,
        loop: true,
        margin: 30,
        dots: false,
        nav: true,
        navText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
        responsive: {
            0: {items: 2},
            479: {items: 2},
            768: {items: 2},
            991: {items: 4},
            1024: {items: 4}
        }
    });

    $(".owl-carousel-5").owlCarousel({
        autoplay: true,
        loop: true,
        margin: 30,
        dots: false,
        nav: true,
        navText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
        responsive: {
            0: {items: 2},
            479: {items: 2},
            768: {items: 2},
            991: {items: 4},
            1024: {items: 4}
        }
    });

	setTimeout( function(){ 
		set_featured_product_box_height();
	},1000 );
});

function set_featured_product_box_height(){
	var max_title=0;
	$('.featured-products .caption-title').each(function(){
        var current_height= parseInt($(this).css('height'));
		if(current_height >= max_title){
			max_title = current_height;
		}
    });
	$('.featured-products .caption-title').css('height',max_title);
}
</script>