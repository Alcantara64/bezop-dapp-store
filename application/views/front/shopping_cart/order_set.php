<style type="text/css">
    #overlay {
        position: fixed;
        display: none;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(0,0,0,0.5);
        z-index: 2;
        cursor: pointer;
    }
</style>
<div id="overlay"></div>
<div class="col-md-8">
    <!-- <?= $this->crud_model->get_individual_cart_amount('3', 'grand_total')."<br>"; ?> -->
    <?php 
        $carted = $this->cart->contents();
        $admin_exists = FALSE;
        $vendors = array();
        foreach ($carted as $items){

            $added_by = json_decode($this->db->get_where('product',array('product_id'=>$items['id']))->row()->added_by,true);
            if($added_by['type'] == 'admin'){
                $admin_exists = TRUE;
                $name = $this->db->get_where('general_settings',array('type'=>'system_name'))->row()->value;
            } else if($added_by['type'] == 'vendor'){
                $vendors[] = $added_by['id'];
                $name = $this->db->get_where('vendor',array('vendor_id'=>$added_by['id']))->row()->display_name;
            }
        }

        if ($admin_exists == TRUE) {
        ?>
            <table class="table table-bordered carter_table" style="background: #fff;">
                <thead>
                    <tr>
                        <th class="hidden-sm hidden-xs"><?php echo translate('image');?></th>
                        <th><?php echo translate('product_details');?></th>
                        <th><?php echo translate('unit_price');?></th>
                        <th style="text-align:center;"><?php echo translate('quantity');?></th>
                        <th><?php echo translate('subtotal');?></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="6">
                            <?=translate('product_by:_')?><a href="<?=base_url()?>"><?= $this->db->get_where('general_settings',array('type'=>'system_name'))->row()->value;?></a>
                        </td>
                    </tr>
                <?php
				$total_product_value = 0;
				$total_shipment_cost = 0;
				$total_tax = 0;
                foreach ($carted as $items) {
                    $added_by_admin = json_decode($this->db->get_where('product',array('product_id'=>$items['id']))->row()->added_by,true);
                    if($added_by_admin['type'] == 'admin') {
                    ?>
                        <tr data-rowid="<?php echo $items['rowid']; ?>" >
                            <td class="image hidden-sm hidden-xs" align="center">
                                <a class="media-link" href="<?php echo $this->crud_model->product_link($items['id']); ?>">
                                    <i class="fa fa-plus"></i>
                                    <img src="<?php echo $items['image']; ?>" width="60" alt=""/>
                                </a>
                            </td>
                            <td class="description">
                                <h4 style="">
                                    <a href="<?php echo $this->crud_model->product_link($items['id']); ?>">
                                        <?php echo $items['name']; ?>
                                    </a>
                                </h4>
                                <hr class="mr0">
                                <?php 
                                    $color = $this->crud_model->is_added_to_cart($items['id'],'option','color'); 
                                    if($color){ 
                                ?>
                                <div style="background:<?php echo $color; ?>; height:15px; width:15px;border-radius:50%;"></div>
                                <hr class="mr0">
                                <?php
                                    }
                                ?>
                                
                                <?php
                                    $all_o = json_decode($items['option'],true);
                                    foreach ($all_o as $l => $op) { 
                                        if($l !== 'color' && $op['value'] !== '' && $op['value'] !== NULL){ 
                                ?> 
                                        <span style="font-size:13px;"><?php echo $op['title']; ?></span>
                                : 
                                <?php 
                                    if(is_array($va = $op['value'])){ 
                                ?>
                                <span style="font-size:13px !important;"><?php echo $va = join(', ',$va); ?></span>
                                <?php
                                    } else {
                                ?>
                                <span style="font-size:13px !important;"><?php echo $va; ?></span>
                                <?php
                                    }
                                ?>
                                <hr class="mr0">
                                <?php
                                        }
                                    }
                                ?>
                                <a href="<?php echo $this->crud_model->product_link($items['id']); ?>" class="change_choice_btn">
                                    <?php echo translate('change_choices'); ?>
                                </a>
                            </td>
                            <td class="quantity pric">
                                <?php echo currency($items['price']); ?>
                            </td>
                            <td class="quantity product-single" style="text-align:center;">
                                <?php
                                    if(!$this->crud_model->is_digital($items['id'])){
                                ?>
                                <span class="buttons">
                                    <div class="quantity product-quantity">
                                        <button type='button' class="btn in_xs quantity-button minus"  value='minus' >
                                            <i class="fa fa-minus"></i>
                                        </button>
                                        <input  type="number" class="form-control qty in_xs quantity-field quantity_field" data-rowid="<?php echo $items['rowid']; ?>" data-limit='no' value="<?php echo $items['qty']; ?>" id='qty1' onblur="check_ok(this);" />
                                        <button type='button' class="btn in_xs quantity-button plus"  value='plus' >
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                </span>
                                <?php
                                    }
                                ?>
                            </td>
                            <td class="total">
                                <span class="sub_total">
                                    <?php 
										echo currency($items['subtotal']); 
										$total_product_value = $total_product_value+$items['subtotal'];
										$total_shipment_cost = $total_shipment_cost+($items['shipping']*$items['qty']);
										$total_tax = $total_tax+($items['tax']*$items['qty']);
									?> 
                                </span>
                            </td>
                            <td class="total">
                                <span class="close" style="color:#f00;">
                                    <i class="fa fa-trash"></i>
                                </span>
                            </td>
                        </tr>
                    <?php
                    }
                }
                ?>
                    <tr>
                        <td colspan="4">
                            <?=translate('total_product_value')?> (<a href="<?=base_url()?>"><?= $this->db->get_where('general_settings',array('type'=>'system_name'))->row()->value;?>)</a>
                        </td>
                        <td colspan="2" class="total">
                            <?= currency($total_product_value); ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <?=translate('total_shipment_cost')?> (<a href="<?=base_url()?>"><?= $this->db->get_where('general_settings',array('type'=>'system_name'))->row()->value;?>)</a>
                        </td>
                        <td colspan="2" class="total">
                            <?= currency($total_shipment_cost); ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <?=translate('total_tax')?> (<a href="<?=base_url()?>"><?= $this->db->get_where('general_settings',array('type'=>'system_name'))->row()->value;?>)</a>
                        </td>
                        <td colspan="2" class="total">
                            <?= currency($total_tax); ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <?=translate('total_payable')?> (<a href="<?=base_url()?>"><?= $this->db->get_where('general_settings',array('type'=>'system_name'))->row()->value;?>)</a>
                        </td>
                        <td colspan="2" class="total">
                            <?= currency($total_product_value+$total_shipment_cost+$total_tax); ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        <?php
        }
        $vendor_count = count($vendors);
        if ($vendor_count > 0) {
			$already_vendor = array();
            foreach ($vendors as $cart_vendor) {
				if(!in_array($cart_vendor,$already_vendor)){
					$already_vendor[] = $cart_vendor;
					$total_product_value = 0;
					$total_shipment_cost = 0;
					$total_tax = 0;
            ?>
                <table class="table table-bordered carter_table" style="background: #fff;">
                    <thead>
                        <tr>
                            <th class="hidden-sm hidden-xs"><?php echo translate('image');?></th>
                            <th><?php echo translate('product_details');?></th>
                            <th><?php echo translate('unit_price');?></th>
                            <th style="text-align:center;"><?php echo translate('quantity');?></th>
                            <th><?php echo translate('subtotal');?></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="6">
                                <?=translate('product_by:_')?><a href="<?=$this->crud_model->vendor_link($cart_vendor)?>"><?= $this->db->get_where('vendor',array('vendor_id'=>$cart_vendor))->row()->display_name;?></a>
                            </td>
                        </tr>
                        <?php
                        foreach ($carted as $items) {
                            $added_by_vendor = json_decode($this->db->get_where('product',array('product_id'=>$items['id']))->row()->added_by,true);
                            if($added_by_vendor['id'] == $cart_vendor) {
                            ?>
                                <tr data-rowid="<?php echo $items['rowid']; ?>" >
                                    <td class="image hidden-sm hidden-xs" align="center">
                                        <a class="media-link" href="<?php echo $this->crud_model->product_link($items['id']); ?>">
                                            <i class="fa fa-plus"></i>
                                            <img src="<?php echo $items['image']; ?>" width="60" alt=""/>
                                        </a>
                                    </td>
                                    <td class="description">
                                        <h4 style="">
                                            <a href="<?php echo $this->crud_model->product_link($items['id']); ?>">
                                                <?php echo $items['name']; ?>
                                            </a>
                                        </h4>
                                        <hr class="mr0">
                                        <?php 
                                            $color = $this->crud_model->is_added_to_cart($items['id'],'option','color'); 
                                            if($color){ 
                                        ?>
                                        <div style="background:<?php echo $color; ?>; height:15px; width:15px;border-radius:50%;"></div>
                                        <hr class="mr0">
                                        <?php
                                            }
                                        ?>
                                        
                                        <?php
                                            $all_o = json_decode($items['option'],true);
                                            foreach ($all_o as $l => $op) { 
                                                if($l !== 'color' && $op['value'] !== '' && $op['value'] !== NULL){ 
                                        ?> 
                                                <span style="font-size:13px;"><?php echo $op['title']; ?></span>
                                        : 
                                        <?php 
                                            if(is_array($va = $op['value'])){ 
                                        ?>
                                        <span style="font-size:13px !important;"><?php echo $va = join(', ',$va); ?></span>
                                        <?php
                                            } else {
                                        ?>
                                        <span style="font-size:13px !important;"><?php echo $va; ?></span>
                                        <?php
                                            }
                                        ?>
                                        <hr class="mr0">
                                        <?php
                                                }
                                            }
                                        ?>
                                        <a href="<?php echo $this->crud_model->product_link($items['id']); ?>" class="change_choice_btn">
                                            <?php echo translate('change_choices'); ?>
                                        </a>
                                    </td>
                                    <td class="quantity pric">
                                        <?php echo currency($items['price']); ?>
                                    </td>
                                    <td class="quantity product-single" style="text-align:center;">
                                        <?php
                                            if(!$this->crud_model->is_digital($items['id'])){
                                        ?>
                                        <span class="buttons">
                                            <div class="quantity product-quantity">
                                                <button type='button' class="btn in_xs quantity-button minus"  value='minus' >
                                                    <i class="fa fa-minus"></i>
                                                </button>
                                                <input  type="number" class="form-control qty in_xs quantity-field quantity_field" data-rowid="<?php echo $items['rowid']; ?>" data-limit='no' value="<?php echo $items['qty']; ?>" id='qty1' onblur="check_ok(this);" />
                                                <button type='button' class="btn in_xs quantity-button plus"  value='plus' >
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                        </span>
                                        <?php
                                            }
                                        ?>
                                    </td>
                                    <td class="total">
                                        <span class="sub_total">
                                            <?php 
												echo currency($items['subtotal']);
												$total_product_value = $total_product_value+$items['subtotal'];
                                                $total_shipment_cost = $total_shipment_cost+($items['shipping']*$items['qty']);
                                                $total_tax = $total_tax+($items['tax']*$items['qty']);
											?> 
                                        </span>
                                    </td>
                                    <td class="total">
                                        <span class="close" style="color:#f00;">
                                            <i class="fa fa-trash"></i>
                                        </span>
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                        ?>
                        <tr>
                            <td colspan="4">
                                <?=translate('total_product_value')?> (<a href="<?=$this->crud_model->vendor_link($cart_vendor)?>"><?= $this->db->get_where('vendor',array('vendor_id'=>$cart_vendor))->row()->display_name;?>)</a>
                            </td>
                            <td colspan="2" class="total">
                                <?= currency($total_product_value); ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <?=translate('total_shipment_cost')?> (<a href="<?=$this->crud_model->vendor_link($cart_vendor)?>"><?= $this->db->get_where('vendor',array('vendor_id'=>$cart_vendor))->row()->display_name;?>)</a>
                            </td>
                            <td colspan="2" class="total">
                                <?= currency($total_shipment_cost); ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <?=translate('total_tax')?> (<a href="<?=$this->crud_model->vendor_link($cart_vendor)?>"><?= $this->db->get_where('vendor',array('vendor_id'=>$cart_vendor))->row()->display_name;?>)</a>
                            </td>
                            <td colspan="2" class="total">
                                <?= currency($total_tax); ?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <?=translate('total_payable')?> (<a href="<?=$this->crud_model->vendor_link($cart_vendor)?>"><?= $this->db->get_where('vendor',array('vendor_id'=>$cart_vendor))->row()->display_name;?>)</a>
                            </td>
                            <td colspan="2" class="total">
                                <?= currency($total_product_value+$total_shipment_cost+$total_tax); ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <?php
				}
            }
        }
    ?>
</div>
<div class="col-md-4">
    <h3 class="block-title">
        <span>
            <?php echo translate('shopping_cart');?>
        </span>
    </h3>
    <div class="shopping-cart" style="background: #fff;">
        <table>
            <tr>
                <td><?php echo translate('subtotal');?>:</td>
                <td  id="total"></td>
            </tr>
            <tr>
                <td><?php echo translate('tax');?>:</td>
                <td id="tax"></td>
            </tr>
            <tr>
                <td><?php echo translate('shipping');?>:</td>
                <td id="shipping"></td>
            </tr>

            <tr class="coupon_disp" <?php if($this->cart->total_discount()<=0){ ?>style="display:none;" <?php } ?>>
                <td><?php echo translate('coupon_discount');?></td>
                <td id="disco">
                    <?php echo currency($this->cart->total_discount()); ?>
                </td>
            </tr>

            <tfoot>
                <tr>
                    <td><?php echo translate('grand_total');?>:</td>
                    <td class="grand_total" id="grand"></td>
                </tr>
            </tfoot>
        </table>


        <?php if($this->cart->total_discount() <= 0 && $this->session->userdata('couponer') !== 'done' && $this->cart->get_coupon() == 0){ ?>
            <h5>
            	<?php echo translate('enter_your_coupon_code_if_you_have_one');?>.
            </h5>
            <div class="form-group">
                <input type="text" class="form-control coupon_code" placeholder="Enter your coupon code">
            </div>
            <span class="btn btn-theme btn-block coupon_btn">
                <?php echo translate('apply_coupon');?>
            </span>
        <?php } else { ?>
            <p>
              <?php echo translate('coupon_already_activated'); ?>
            </p>
        <?php } ?>
    </div>

</div>

<div class="col-md-12">
    <span class="btn btn-theme-dark" onclick="load_address_form();">
        <?php echo translate('next');?>
    </span>
</div>


<?php
    echo form_open('', array(
    'method' =>
    'post', 'id' => 'coupon_set' )); 
?> 
<input type="hidden" id="coup_frm" name="code">
</form>

<script type="text/javascript">
    $( document ).ready(function() {	
        update_calc_cart();
    });
</script>

