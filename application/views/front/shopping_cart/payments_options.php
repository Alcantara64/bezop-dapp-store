<?php 
	$cart_vendor_totals = $this->crud_model->cart_vendor_totals();
	foreach($cart_vendor_totals as $vendor=>$amount){
?>
    <?php 
        $system_title = $this->db->get_where('general_settings',array('type' => 'system_title'))->row()->value;
        $total = $this->cart->total(); 
        if ($this->crud_model->get_type_name_by_id('business_settings', '3', 'value') == 'product_wise') { 
            $shipping = $this->crud_model->cart_total_it('shipping'); 
        } elseif ($this->crud_model->get_type_name_by_id('business_settings', '3', 'value') == 'fixed') { 
            $shipping = $this->crud_model->get_type_name_by_id('business_settings', '2', 'value'); 
        } 
        $tax = $this->crud_model->cart_total_it('tax'); 
        //$grand = $total + $shipping + $tax; 
		$grand = $amount;
        //echo $grand; 
              $eth_amount = $this->crud_model->convert_dollar_eth($grand);
              $price_currency = "0x0000000000000000000000000000000000000000";
    ?>
    <?php
        
		if($vendor == 'admin'){
            $p_set = $this->db->get_where('business_settings',array('type'=>'paypal_set'))->row()->value; 
            $c_set = $this->db->get_where('business_settings',array('type'=>'cash_set'))->row()->value; 
            $s_set = $this->db->get_where('business_settings',array('type'=>'stripe_set'))->row()->value;
            $c2_set = $this->db->get_where('business_settings',array('type'=>'c2_set'))->row()->value; 
            $vp_set = $this->db->get_where('business_settings',array('type'=>'vp_set'))->row()->value; 
            $mt = $this->db->get_where('admin',array('role'=>'1'))->row()->metamask_address;
            //mtaddress = $this->db->get_where('business_settings',array('type'=>'metamask'))->row()->value;
			$char = $this->db->get_where('general_settings',array('type'=>'system_name'))->row()->value; 
		echo $name =str_replace(' ','',$char);
                        
                } else {
            $p_set = $this->db->get_where('vendor',array('vendor_id'=>$vendor))->row()->paypal_set; 
            $c_set = $this->db->get_where('vendor',array('vendor_id'=>$vendor))->row()->cash_set; 
            $s_set = $this->db->get_where('vendor',array('vendor_id'=>$vendor))->row()->stripe_set;
            $c2_set = $this->db->get_where('vendor',array('vendor_id'=>$vendor))->row()->c2_set; 
            $vp_set = $this->db->get_where('vendor',array('vendor_id'=>$vendor))->row()->vp_set; 
            $mt = $this->db->get_where('vendor',array('vendor_id'=>$vendor))->row()->metamask_address; 
			$name = $this->db->get_where('vendor',array('vendor_id'=>$vendor))->row()->display_name; 
		}
    ?> 
    <div class="row">
        <div class="cc-selector col-sm-12">
            <?php echo translate('payment_for');?> <?php echo $name.": "; ?><b><?php echo currency($amount); ?></b>
        </div>
       
        </script>
         <?php
             if($mt.length > 1){
        ?>
        <div class="cc-selector col-sm-3">
            <input id="mastercardc8_<?= $vendor; ?>" style="display:block;" checked type="radio" name="payment_type_<?= $vendor; ?>" value="bezop"/>
            <label class="drinkcard-cc" style="margin-bottom:0px; width:100%; overflow:hidden;" for="mastercardc5_<?= $vendor; ?>" onclick="radio_check('mastercardc5_<?= $vendor; ?>')">
                    <img src="<?php echo base_url(); ?>template/front/img/preview/payments/besopblok.png" width="100%" height="100%" style=" text-align-last:center;" alt="<?php echo translate('besop_blockchain');?>" />
                   
            </label>
        </div>
        
        <?php
                       
            } 
        ?>
        
        
    </div>
    <div class="row">
        <div class="col-sm-12" style="padding-bottom: 15px;border-bottom: 1px solid #e0e0e0;">
            <span id="order_place_btn_<?= str_replace(' ','',$name); ?>"  class="btn btn-theme pull-right order_place_btn" >
            <?php echo translate('place_order_for');?> <?= $name; ?>
            </span>
        </div>
    </div>
<script>
$(document).ready(function(){
    $('#order_place_btn_<?php echo str_replace(' ','',$char);?>').on('click', function(){
        var status =buyProduct('<?php echo $mt?>', '0x<?php echo hash("sha256", $mt.$grand);?>', '<?php echo $price_currency ?>', '<?php echo $eth_amount?>');
        if(status > 0){
    cart_submission('<?php echo $vendor; ?>');
        
        }
        
    });
});
</script>
    
<?php 
	}
?>
<input type="hidden" value="" id="curr_idv" name="curr_idv" />
<style>
.cc-selector input{
    margin:0;padding:0;
    -webkit-appearance:none;
       -moz-appearance:none;
            appearance:none;
}
 
.cc-selector input:active +.drinkcard-cc
{
	opacity: 1;
	border:4px solid #169D4B;
}
.cc-selector input:checked +.drinkcard-cc{
	-webkit-filter: none;
	-moz-filter: none;
	filter: none;
	border:4px solid #284262;
}
.drinkcard-cc{
	cursor:pointer;
	background-size:contain;
	background-repeat:no-repeat;
	display:inline-block;
	-webkit-transition: all 100ms ease-in;
	-moz-transition: all 100ms ease-in;
	transition: all 100ms ease-in;
	-webkit-filter:opacity(.5);
	-moz-filter:opacity(.5);
	filter:opacity(.5);
	transition: all .6s ease-in-out;
	border:4px solid transparent;
	border-radius:5px !important;
}
.drinkcard-cc:hover{
	-webkit-filter:opacity(1);
	-moz-filter: opacity(1);
	filter:opacity(1);
	transition: all .6s ease-in-out;
	border:4px solid #00bffe;
			
}

</style>