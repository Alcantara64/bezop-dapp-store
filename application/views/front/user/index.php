  <?php $CURRENCIES = array(
	"0x0000000000000000000000000000000000000000" => "ETH",
	"0x253b90f42684f0d56dacf6ea9dcd3982f73e834e" => "TEST"
);?>
<script> var CONTRACT_ADDRESS = "0x013c65a7c0726ffc92eb3904cb17a0040d5991b5";</script>
<script src="<?php echo base_url()?>escrow/abi.js"></script>
<script src="<?php echo base_url()?>escrow/web3.js"></script>


<script src="<?php echo base_url()?>escrow/main.js"></script>
<section class="page-section profile_top">
    <div class="wrap container">
    	<div class="row">
            <div class="col-md-10">
                <div class="top_nav">
                    <ul>
                        <li class="active">
                            <span id="info">
                                <?php echo translate('profile');?>
                            </span>
                        </li>
                        <li>
                        	<span id="wishlist">
                        		<?php echo translate('wishlist');?>
                        	</span>
                        </li>
                        <li>
                        	<span id="order_history">
                        		<?php echo translate('order_history');?>
                        	</span>
                        </li>
                         <li>
                        	<span id="transactions">
                        		<?php echo translate('transactions');?>
                        	</span>
                        </li>
                        <li>
                        	<span id="arbitration">
                        		<?php echo translate('my_arbitration(s)');?>
                        	</span>
                        </li>
                        <li>
                        	<span id="update_profile">
                        		<?php echo translate('edit_profile');?>
                        	</span>
                        </li>
                        <li>
                        	<span id="dispute">
                        		<?php echo translate('my_dispute');?>
                        	</span>
                        </li>
                        <li>
                        	<span id="ticket">
                        		<?php echo translate('support_ticket');?>
                        	</span>
                        </li>
                         <li>
                        	<span id="arbitrationsc">
                        		<?php echo translate('arbitration(s)');?>
                        	</span>
                        </li>
                      </ul>
                </div>
            </div>
            <div class="col-md-2">
                <div class="top_nav">
                    <ul>
                        <li><a style="color:#F00;" href="<?php echo base_url(); ?>home/logout/">logout</a></li>
                     </ul>
                </div>
            </div>
        </div>
	</div>
</section>
<hr class="hr_sp">
<section class="page-section">
    <div class="wrap container">
            <table id="balancesTable">
	</table>

        <div id="profile-content">
        </div>
    </div>
</section>
<script>
	var top = Number(200);
	var loading_set = '<div style="text-align:center;width:100%;height:'+(top*2)+'px; position:relative;top:'+top+'px;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>';
	
	$('#info').on('click',function(){
		$("#profile-content").html(loading_set);
		$("#profile-content").load("<?php echo base_url()?>home/profile/info");
		$("li").removeClass("active");
		$(this).closest("li").addClass("active");
	});
	$('#wishlist').on('click',function(){
		$("#profile-content").html(loading_set);
		$("#profile-content").load("<?php echo base_url()?>home/profile/wishlist");
		$("li").removeClass("active");
		$(this).closest("li").addClass("active");
	});
	$('#order_history').on('click',function(){
		$("#profile-content").html(loading_set);
		$("#profile-content").load("<?php echo base_url()?>home/profile/order_history");
		$("li").removeClass("active");
		$(this).closest("li").addClass("active");
	});
	$('#arbitration').on('click',function(){
		$("#profile-content").html(loading_set);
		$("#profile-content").load("<?php echo base_url()?>home/profile/arbitration");
		$("li").removeClass("active");
		$(this).closest("li").addClass("active");
	});
        $('#transactions').on('click',function(){
		$("#profile-content").html(loading_set);
		$("#profile-content").load("<?php echo base_url()?>home/profile/transaction");
		$("li").removeClass("active");
		$(this).closest("li").addClass("active");
	});
	$('#dispute').on('click',function(){
		$("#profile-content").html(loading_set);
		$("#profile-content").load("<?php echo base_url()?>home/profile/dispute");
		$("li").removeClass("active");
		$(this).closest("li").addClass("active");
	});
        $('#update_profile').on('click',function(){
		$("#profile-content").html(loading_set);
		$("#profile-content").load("<?php echo base_url()?>home/profile/update_profile");
		$("li").removeClass("active");
		$(this).closest("li").addClass("active");
	});
	$('#ticket').on('click',function(){
		$("#profile-content").html(loading_set);
		$("#profile-content").load("<?php echo base_url()?>home/profile/ticket");
		$("li").removeClass("active");
		$(this).closest("li").addClass("active");
	});
        $('#arbitrationsc').on('click',function(){
		$("#profile-content").html(loading_set);
		$("#profile-content").load("<?php echo base_url()?>home/profile/arbitrationc");
		$("li").removeClass("active");
		$(this).closest("li").addClass("active");
	});
	$('#message_view').on('click',function(){
		$("#profile-content").html(loading_set);
		$("#profile-content").load("<?php echo base_url()?>home/profile/message_view");
	});
	$(document).ready(function(){
		$("#<?php echo $part; ?>").click();
    });
</script>
<script>
/* JavaScript global constants */
var ALL_REVIEWED_TRADE_IDS = {<?php
$ids = $this->db->query("SELECT trade_index FROM reviews");
foreach ($ids->result() as $id)
{
	echo $id["trade_index"].": true, ";
}
?>
"": true};
var CONTRACT_ADDRESS = "<?php echo $CONTRACT_ADDRESS;?>";
var CURRENCIES = {
	<?php
	foreach ($CURRENCIES as $contractAddress => $symbol)
	{
		echo "'$contractAddress': '$symbol',";
	}
	?>
};

</script>
<style type="text/css">
    .pagination_box a{
        cursor: pointer;
    }
</style>