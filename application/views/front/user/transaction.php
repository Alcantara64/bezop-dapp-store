<div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Special title treatment</h3>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <span onclick="deposit(currency)" class="btn btn-primary"><?php echo translate('withdrawal');?></span>
            </div>
        </div>
    </div>
    <!--/.Panel-->

    <!--Panel-->
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Special title treatment</h3>
                <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                <span onclick="withdraw()" class="btn btn-primary"><?php echo translate('deposit');?></span>
            </div>
        </div>
    </div>
    <table id="balancesTable">
	</table>
    <div id="sellerAddress"></div>
    <!--/.Panel-->
</div>
