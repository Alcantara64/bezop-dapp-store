<?php
	$vendor_categories = $this->crud_model->vendor_categories($vendor_id);
	if(count($vendor_categories) !== 0) {
    ?>
    <div class="row">
        <div class="col-sm-9">
            <?php
                //print_r($vendor_categories);
            	foreach($vendor_categories as $category_id){
            		$digital_ckeck=$this->db->get_where('category',array('category_id'=>$category_id))->row()->digital;
            		if($this->crud_model->if_publishable_category($category_id)) {
                        $vendor_products_by_cat = $this->crud_model->vendor_products_by_cat($vendor_id, $category_id);
                        $i=1;
                        ?>  
                        <div class="row">
                            <div class="col-sm-12">
                                <h2 class="section-title section-title-lg">
                                    <span>
                                        <span class="thin"> <?php echo $this->crud_model->get_type_name_by_id('category', $category_id, 'category_name');?></span>
                                    </span>
                                </h2>
                                <div class="featured-products-carousel">
                                    <div class="owl-carousel-<?=$i?>" id="recently-viewed-carousel">
                                    <?php
                                        foreach ($vendor_products_by_cat as $products_by_cat) {
                                            echo $this->html_model->product_box($products_by_cat, 'grid', '2');
                                        }
                                    ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            $(document).ready(function(){
                                $(".owl-carousel-<?=$i?>").owlCarousel({
                                    autoplay: true,
                                    loop: true,
                                    margin: 30,
                                    dots: false,
                                    nav: true,
                                    navText: [
                                        "<i class='fa fa-angle-left'></i>",
                                        "<i class='fa fa-angle-right'></i>"
                                    ],
                                    responsive: {
                                        0: {items: 2},
                                        479: {items: 2},
                                        768: {items: 2},
                                        991: {items: 4},
                                        1024: {items: 4}
                                    }
                                });
                            });
                        </script>
                    <?php
            		}
                    $i++;
            	}
            ?>
        </div>
        <div class="col-sm-3">
            <?php
                foreach($vendor_categories as $row) {
                        if($this->crud_model->if_publishable_category($row['category'])) {
                    ?>
                        <?php 
                            if(file_exists('uploads/category_image/'.$this->crud_model->get_type_name_by_id('category',$row['category'],'banner'))){
                                $img_url = base_url()."uploads/category_image/".$this->crud_model->get_type_name_by_id('category',$row['category'],'banner'); 
                            } else {
                                $img_url = base_url()."uploads/category_image/default.jpg"; 

                            }
                        ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div style="height: 250px; width: 100%; background-image: url(<?=$img_url?>); background-position-x: center; background-position-y: top; background-repeat: no-repeat;">
                                    <div class="p-item p-item-type-zoom">
                                        <span class="p-item-hover">
                                            <div class="p-item-info">
                                                <div class="p-headline">
                                                    <span><?php echo $this->crud_model->get_type_name_by_id('category',$row['category'],'category_name'); ?></span>
                                                    <div class="p-line"></div>
                                                    <div class="p-btn">
                                                        <a href="<?php echo base_url(); ?>home/vendor_category/<?=$vendor_id?>/<?php echo $row['category']; ?>" class="btn  btn-theme-transparent btn-theme-xs"><?php echo translate('browse'); ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="p-mask" style="height: 250px"></div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                }
                ?>
        </div>
    </div>
    <?php
    }
?>