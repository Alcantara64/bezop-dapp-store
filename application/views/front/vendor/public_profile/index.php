<?php 
	include 'header.php';
?>
<?php 
	include $content.'/index.php';
?>
<style>
    .social-icons a {
        display: block;
        position: relative;
        width: 30px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        background-color: transparent;
        border: solid 1px #ccc;
        color: #ccc;
    }
    .caption-title {
        height: 61px !important;
    }
</style>